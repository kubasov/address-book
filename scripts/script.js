let Contact = React.createClass({
  getInitialState: function() {
    return {
      editing: false,
      inFavourites: false
    }
  },
  edit: function() {
    this.setState({editing: true})
  },
  save: function() {
    this.props.editSingleContact(this.refs.nameText.value, this.refs.phoneText.value, this.props.index);
    this.setState({editing:false})
  },
  remove: function () {
    this.props.deleteFromList(this.props.index);
  },
  markFavourites: function() {
    this.setState({inFavourites: !this.state.inFavourites})
  },
  renderEdit: function() {
    return <li key={this.props.id} data-inFavourites={this.state.inFavourites} data-editing={this.state.editing}>
      <img src={this.props.image} width="60px" height="60px"/>
      <textarea ref='nameText' defaultValue={this.props.name}></textarea>
      <textarea ref='phoneText' defaultValue={this.props.phone}></textarea>
      <button onClick={this.save} className='edit-btn'>Save</button>
    </li>
  },
  renderNormal: function() {
    return <li key={this.props.id} data-inFavourites={this.state.inFavourites} data-editing={this.state.editing}>
      <img src={this.props.image} width="60px" height="60px"/>
      <div>{this.props.name}</div>
      <div>{this.props.phone}</div>
      <button onClick={this.edit} className='edit-btn'>Edit</button>
      <button onClick={this.markFavourites} className='star-btn'>Star</button>
      <button onClick={this.remove} className='star-btn'>Delete</button>
    </li>
  },
  render: function () {
    return (this.state.editing==true) ? this.renderEdit() : this.renderNormal();
  }
})
///////////////////////////////////////////////////////////////////////////////
let ContactsList = React.createClass({
  getInitialState: function () {
    var _this=this;
    $.get('/getusers', function(result){
      var CONTACTS = result;
      _this.setState({displayedContacts:CONTACTS, allContacts:CONTACTS});
    })
    return {
      displayedContacts : [] || CONTACTS,
      allContacts: [] || CONTACTS
    }
  },
  inputHandler: function(event){
    var searchQuery = event.target.value.toLowerCase(),
        displayedContacts = this.state.allContacts.filter(function(el){
          let searchValue = el.name.toLowerCase();
          return searchValue.indexOf(searchQuery) !== -1;
        })
    this.setState({
      displayedContacts
    })
  },
  sortContacts: function() {
    this.state.displayedContacts.sort(
      function(a,b){
        if (a.name < b.name)
          return -1;
        if (a.name > b.name)
          return 1;
        return 0;
      }
    );
  },
  removeContact: function (index) {
    let arr = this.state.allContacts;
    arr.splice(index,1);
    this.updateList({allContacts:arr});
  },
  editContact: function (nameText, phoneText, i) {
    let arr = this.state.allContacts;
    arr[i].name = nameText;
    arr[i].phone = phoneText;
    this.updateList({allContacts:arr});
  },
  addContact: function (contact) {
    let arr = this.state.allContacts;
    arr.push(contact);
    this.updateList({allContacts:arr});
  },
  eachContact: function(el, index) {
    return <Contact key={index}
                    index={index}
                    name={el.name}
                    phone={el.phone}
                    image={el.image}
                    deleteFromList={this.removeContact}
                    editSingleContact={this.editContact}
            />
  },
  updateList: function (obj) {
    this.setState(obj);
    console.log(this.state.allContacts);
    $.post('/newuser', {contacts:this.state.allContacts});
  },
  render: function () {
    this.sortContacts();
    return <div>
            <input type="text" onChange={this.inputHandler} />
            <ul>{
                this.state.displayedContacts.map(this.eachContact)
              }
            </ul>
            <button onClick={this.addContact.bind(null, {
              name:'Новый контакт',
              phone:'111111',
              image: 'https://pp.vk.me/c633724/v633724347/4ba09/_YUD4fx9P70.jpg'
            })}>Add new</button>
          </div>;
  }
})

ReactDOM.render(
  <ContactsList />,
  document.getElementById('content')
);
