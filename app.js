var express = require('express');
var app = express();
var fs = require('fs');
var bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/scripts',express.static(__dirname + '/scripts'));

app.get('/', function(req,res){
  res.sendfile(__dirname + '/html/index.html');
})
////////////////////////////////////////
app.get('/getusers', function(req,res){
  var obj;
  fs.readFile(__dirname+'/data/users.json', 'utf8', function (err, data) {
    if (err) throw err;
    obj = JSON.parse(data);
    res.send(obj);
  });
})
app.post('/newuser' ,function (req, res) {
  var contacts = JSON.stringify(req.body.contacts);
  fs.writeFile(__dirname+'/data/users.json', contacts, 'utf-8', function(err){
    if (err) throw err;
    console.log('file saved');
  })
})
////////////////////////////////////////
app.listen('7287', function(){
  console.log('server started');
})
